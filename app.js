import express from "express";
import { promisify } from "util";
import route from "./src/routes/routes.js";
import slip from "./src/routes/slip.js";
import cadence from "./src/routes/cadence.js";
import unit from "./src/routes/unit.js";
import auth from "./src/routes/authorization.js";
import "./src/model/index.js";
import bodyParser from "body-parser";
import cors from "cors";
import db from "./src/model/index.js";

const app = express();
const port = 3001;

app.use(cors());
app.use(bodyParser.json());

app.use("/routes", route);
app.use("/slip", slip);
app.use("/cadence", cadence);
app.use("/unit", unit);
app.use("/auth", auth);

db.sequelize.sync();

app.listen(port, async () => {
  console.log(`Servidor esta escutando a porta ${port}.`);
});
