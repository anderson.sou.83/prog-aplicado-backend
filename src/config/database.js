const config  = {  
    dialect: "postgres",
    host: "127.0.0.1",  
    username: "postgres",
    password: 123456,    
    database: "dbIgti",      
    define: {
        timestamp:true,
    },
};

export default config;