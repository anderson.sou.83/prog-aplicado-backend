const { Model, Sequelize } = require("sequelize");

class route extends Model {
  static init(connection) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        slipNumber: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        transportNumber: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        numberRoute: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        sourceUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        targetUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        storeHouseUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        supplierUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        shipToUnitId: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      },
      {
        sequelize: connection,
        modelName: "route",
      }
    );
  }
}

module.exports = route;
