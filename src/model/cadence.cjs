const { Model, Sequelize } = require("sequelize");

class cadence extends Model {
  static init(connection) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        routeId: Sequelize.INTEGER,
        weekNumber: Sequelize.INTEGER,
        year: Sequelize.INTEGER,
        Monday: { type: Sequelize.DECIMAL },
        Tuesday: { type: Sequelize.DECIMAL },
        Wednesday: { type: Sequelize.DECIMAL },
        Thursday: { type: Sequelize.DECIMAL },
        Friday: { type: Sequelize.DECIMAL },
        Saturday: { type: Sequelize.DECIMAL },
        Sunday: { type: Sequelize.DECIMAL },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      },
      {
        sequelize: connection,
        modelName: "cadence",
      }
    );
  }
}

module.exports = cadence;
