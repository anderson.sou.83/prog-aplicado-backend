const { Model, Sequelize } = require("sequelize");

class Slip extends Model {
  static init(connection) {
    super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        numberSlip: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        numberTn: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        sourceUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        storeHouseUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        supplierUnitId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize: connection,
        modelName: "slip",
      }
    );
  }
}

module.exports = Slip;
