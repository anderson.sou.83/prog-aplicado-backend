import Sequelize from "sequelize";
import config from "../config/database.js";
import Cadence from "./cadence.cjs";
import Route from "./route.cjs";
import Unit from "./unit.cjs";
import Slip from "./slip.cjs";
import User from "./user.cjs";

const db = {};
const sequelize = new Sequelize(config);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

Cadence.init(sequelize);
Unit.init(sequelize);
Slip.init(sequelize);
Route.init(sequelize);
User.init(sequelize);

Route.hasMany(Cadence, {
  foreignKey: "routeId",
  as: "route",
  sourceKey: "id",
});

Cadence.belongsTo(Route, {
  foreignKey: "routeId",
  as: "route",
  targetKey: "id",
});

Unit.hasMany(Route, {
  foreignKey: "sourceUnitId",
  as: "sourceUnitRoute",
  sourceKey: "id",
});

Route.belongsTo(Unit, {
  foreignKey: "sourceUnitId",
  as: "sourceUnitRoute",
  targetKey: "id",
});

Unit.hasMany(Route, {
  foreignKey: "storeHouseUnitId",
  as: "storeHouseUnitRoute",
  sourceKey: "id",
});

Route.belongsTo(Unit, {
  foreignKey: "storeHouseUnitId",
  as: "storeHouseUnitRoute",
  targetKey: "id",
});

Unit.hasMany(Route, {
  foreignKey: "supplierUnitId",
  as: "supplierUnitRoute",
  sourceKey: "id",
});

Route.belongsTo(Unit, {
  foreignKey: "supplierUnitId",
  as: "supplierUnitRoute",
  targetKey: "id",
});

Unit.hasMany(Route, {
  foreignKey: "targetUnitId",
  as: "targetUnitRoute",
  sourceKey: "id",
});

Route.belongsTo(Unit, {
  foreignKey: "targetUnitId",
  as: "targetUnitRoute",
  targetKey: "id",
});

Unit.hasMany(Route, {
  foreignKey: "shipToUnitId",
  as: "shipToUnitRoute",
  sourceKey: "id",
});
Route.belongsTo(Unit, {
  foreignKey: "shipToUnitId",
  as: "shipToUnitRoute",
  targetKey: "id",
});

Unit.hasMany(Slip, {
  foreignKey: "sourceUnitId",
  as: "sourceUnit",
  sourceKey: "id",
});

Slip.belongsTo(Unit, {
  foreignKey: "sourceUnitId",
  as: "sourceUnit",
  targetKey: "id",
});

Unit.hasMany(Slip, {
  foreignKey: "storeHouseUnitId",
  as: "storeHouseUnit",
  sourceKey: "id",
});

Slip.belongsTo(Unit, {
  foreignKey: "storeHouseUnitId",
  as: "storeHouseUnit",
  targetKey: "id",
});

Unit.hasMany(Slip, {
  foreignKey: "supplierUnitId",
  as: "supplierUnit",
  sourceKey: "id",
});

Slip.belongsTo(Unit, {
  foreignKey: "supplierUnitId",
  as: "supplierUnit",
  targetKey: "id",
});

User.hasMany(Route, { foreignKey: "userId" });

export default db;
