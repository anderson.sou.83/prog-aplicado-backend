const {Model, Sequelize} = require('sequelize');

class User extends Model{
    static init(connection){
        super.init({
            id:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,            
            },
            name:Sequelize.STRING,
            email:Sequelize.STRING,
            password:Sequelize.STRING,
            createdAt: Sequelize.DATE,            
            updatedAt: Sequelize.DATE,
        },
        {
            sequelize : connection,
            modelName: 'user'
        }
        )
    }
}

module.exports = User;