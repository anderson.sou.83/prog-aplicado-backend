import express from "express";
import jwt from "jsonwebtoken";
import User from "../model/user.cjs";

const authorization = express.Router();

authorization.post("/", async (req, res) => {
  try {
    process.env.SECRET = "LmdxqEfEBg25nibx4vtRg11i8UJ3GCgZ";

    const emailUser = req.headers.email;
    const password = req.headers.password;

    const userFind = await User.findOne({ where: { email: emailUser } });

    if (userFind === null) {
      res.status(203).send({ error: "Usuário ou senha invalidos!" });
    }

    if (userFind.password != password) {
      res.status(203).send({ error: "Usuário ou senha invalidos!" });
    }

    const token = jwt.sign({ user: userFind.id }, process.env.SECRET);

    res.send({ token });
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

export default authorization;
