import express from "express";
import { verifyToken } from "../config/auth.js";
import rout from "../model/route.cjs";
import bodyParser from "body-parser";
import UnitModel from "../model/unit.cjs";
import sequelize from "sequelize";

const routes = express.Router();
routes.use(express.json());
routes.use(bodyParser.urlencoded({ extended: false }));
routes.use(bodyParser.json());

routes.get("/", verifyToken, async (req, res) => {
  try {
    const routsFind = await rout.findAll({
      where: req.query,
      include: [
        { model: UnitModel, as: "sourceUnitRoute" },
        { model: UnitModel, as: "storeHouseUnitRoute" },
        { model: UnitModel, as: "supplierUnitRoute" },
        { model: UnitModel, as: "targetUnitRoute" },
        { model: UnitModel, as: "shipToUnitRoute" },
      ],
    });
    res.status(200).json(routsFind);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

routes.post("/", verifyToken, async (req, res) => {
  try {

    const rota = await rout.create({
      slipNumber: req.body.slipNumber,
      transportNumber: req.body.transportNumber,
      numberRoute: req.body.numberRoute,
      sourceUnitId: req.body.sourceUnitId,
      targetUnitId: req.body.targetUnitId,
      storeHouseUnitId: req.body.storeHouseUnitId,
      supplierUnitId: req.body.supplierUnitId,
      shipToUnitId: req.body.shipToUnitId,
      userId: req.body.userId,
    });

    if (rota) {
      res.send(rota);
    } else {
      res.status(400).send("Error in insert new record");
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

routes.put("/", verifyToken, async (req, res) => {
  try {
    const rota = await rout.update(
      {
        slipNumber: req.body.slipNumber,
        transportNumber: req.body.transportNumber,
        numberRoute: req.body.numberRoute,
        sourceUnitId: req.body.sourceUnitId,
        targetUnitId: req.body.targetUnitId,
        storeHouseUnitId: req.body.storeHouseUnitId,
        supplierUnitId: req.body.supplierUnitId,
        shipToUnitId: req.body.shipToUnitId,
        userId: req.body.userId,
      },
      { where: { id: req.body.id } }
    );

    if (rota) {
      res.send(rota);
    } else {
      res.status(400).send("Error in insert new record");
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

export default routes;
