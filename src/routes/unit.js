import express from "express";
import bodyParser from "body-parser";
import { verifyToken } from "../config/auth.js";
import UnitModel from "../model/unit.cjs";
import sequelize from "sequelize";

const unit = express.Router();

unit.use(express.json());
unit.use(bodyParser.urlencoded({ extended: false }));
unit.use(bodyParser.json());

unit.get("/", verifyToken, async (req, res) => {
  try {

    const nameUnit = req.query.name;

    const objUnits = await UnitModel.findAll({
      where: {
        name: {
          [sequelize.Op.like]: sequelize.literal(
            `'%${nameUnit ? nameUnit : ""}%'`
          ),
        },
      },
    });

    if (objUnits === null) {
      res.status(204);
    } else {
      res.status(200).json(objUnits);
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

export default unit;
