import express from "express";
import bodyParser from "body-parser";
import { verifyToken } from "../config/auth.js";
import SlipModel from "../model/slip.cjs";
import UnitModel from "../model/unit.cjs";
import sequelize from "sequelize";

const slip = express.Router();
slip.use(express.json());
slip.use(bodyParser.urlencoded({ extended: false }));
slip.use(bodyParser.json());

slip.get("/", verifyToken, async (req, res) => {
  try {
    const numSlip = req.query.numberSlip;

    const slip = await SlipModel.findAll({
      include: [
        { model: UnitModel, as: "sourceUnit" },
        { model: UnitModel, as: "storeHouseUnit" },
        { model: UnitModel, as: "supplierUnit" },
      ],
      where: {
        numberSlip: {
          [sequelize.Op.like]: sequelize.literal(
            `'%${numSlip ? numSlip : ""}%'`
          ),
        },
      },
    });

    if (slip === null) {
      res.status(204).json({ slip: "Não foi localizado!" });
    } else {
      res.status(200).json(slip);
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

slip.post("/", verifyToken, async (req, res) => {
  try {
    const numSlip = req.body.json();

    const slip = await SlipModel.findOne({
      include: [
        { model: UnitModel, as: "sourceUnit" },
        { model: UnitModel, as: "storeHouseUnit" },
        { model: UnitModel, as: "supplierUnit" },
      ],
      where: { numberSlip: numSlip },
    });

    if (slip === null) {
      res.status(204).json({ slip: "Não foi localizado!" });
    } else {
      res.status(200).json(slip);
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

export default slip;
