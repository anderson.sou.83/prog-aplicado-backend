import express from "express";
import bodyParser from "body-parser";

import { verifyToken } from "../config/auth.js";
import cadenceMod from "../model/cadence.cjs";
import Rout from "../model/route.cjs";

const cadenceRoute = express.Router();
cadenceRoute.use(express.json());
cadenceRoute.use(bodyParser.urlencoded({ extended: false }));
cadenceRoute.use(bodyParser.json());

cadenceRoute.get("/", verifyToken, async (req, res) => {
  try {
    const cadences = await cadenceMod.findAll({
      include: [{ model: Rout, as: "route" }],
      where: req.query,
    });
    res.status(200).json(cadences);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

cadenceRoute.post("/", verifyToken, async (req, res) => {
  try {
    const cadence = await cadenceMod.create({
      id: req.body.id,
      routeId: req.body.routeId,
      weekNumber: req.body.weekNumber,
      year: req.body.year,
      Monday: req.body.Monday,
      Tuesday: req.body.Tuesday,
      Wednesday: req.body.Wednesday,
      Thursday: req.body.Thursday,
      Friday: req.body.Friday,
      Saturday: req.body.Saturday,
      Sunday: req.body.Sunday,
    });

    if (cadence) {
      res.send(cadence);
    } else {
      res.status(400).send("Error in insert new record");
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

cadenceRoute.put("/", verifyToken, async (req, res) => {
  try {
    const cadence = await cadenceMod.update(
      {
        routeId: req.body.routeId,
        weekNumber: req.body.weekNumber,
        year: req.body.year,
        Monday: req.body.Monday,
        Tuesday: req.body.Tuesday,
        Wednesday: req.body.Wednesday,
        Thursday: req.body.Thursday,
        Friday: req.body.Friday,
        Saturday: req.body.Saturday,
        Sunday: req.body.Sunday,
      },
      { where: { id: req.body.id } }
    );

    if (cadence) {
      res.send(cadence);
    } else {
      res.status(400).send("Error in insert new record");
    }
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

export default cadenceRoute;
